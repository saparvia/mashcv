<%def name='render_single_(results, renderers, default=None)'>
	<%
		if not isinstance(results, list):
			results = [results]
	%>

	% if len(results) == 0:
		${default}
	% elif len(results) == 1:
		<%
		typ = value(results[0][0], RDF.type)[0][0]
		%>
		<span title='${results[0].meta(META.sourceService)}'>${renderers.get(typ, lambda x: str(x))(results[0][0])}</span>
	% else:
		<select>
			% for result in results:
			<option>${render_single_(result, renderers)}</option>
			% endfor
		</select>
	% endif
</%def>
