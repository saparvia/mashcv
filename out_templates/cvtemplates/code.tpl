<div class='section-basicinfo' data-render='/render?template=basicinfo&session_id=${SESSION_ID}'><%include file='basicinfo.tpl' args='settings=ALL_SETTINGS.get("triple_list", {})/></div>
<div class='section-workhistory' data-render='/render?template=workhistory&session_id=${SESSION_ID}'><%include file='workhistory.tpl' args='settings=ALL_SETTINGS.get("work_history", {})/></div>
<div class='section-education' data-render='/render?template=education&session_id=${SESSION_ID}'><%include file='education.tpl' args='settings=ALL_SETTINGS.get("education", {})/></div>
<div class='section-projects' data-render='/render?template=projects&session_id=${SESSION_ID}'><%include file='projects.tpl' args='settings=ALL_SETTINGS.get("projects", {})/></div>
<div class='section-proglangs' data-render='/render?template=proglangs&session_id=${SESSION_ID}'><%include file='proglangs.tpl' args='settings=ALL_SETTINGS.get("proglangs", {})/></div>
