<div class='section-basicinfo' data-render='/render?template=basicinfo&session_id=${SESSION_ID}'><%include file='basicinfo.tpl' args='settings=ALL_SETTINGS.get("basicinfo", {})'/></div>
<div class='section-workhistory' data-render='/render?template=workhistory&session_id=${SESSION_ID}'><%include file='workhistory.tpl' args='settings=ALL_SETTINGS.get("education", {})'/></div>
<div class='section-education' data-render='/render?template=education&session_id=${SESSION_ID}'><%include file='education.tpl' args='settings=ALL_SETTINGS.get("education", {})'/></div>
<div class='section-publications' data-render='/render?template=publications&session_id=${SESSION_ID}'><%include file='publications.tpl' args='settings=ALL_SETTINGS.get("publications", {})'/></div>
