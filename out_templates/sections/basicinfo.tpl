<%namespace file='render.tpl' import='render_single'/>

<h2 class='sectionheading'>Basic info</h2>
<ul>
	<li><span>Name: </span>${render_single(values(s=SUBJECT, p=FOAF.name))}</li>
	<li><span>Email: </span>${render_single(values(s=SUBJECT, p=FOAF.mbox))}</li>
	<li><span>PGP key: </span>${render_single(values(s=SUBJECT, p=WOT.hasKey))}</li>
</ul>
