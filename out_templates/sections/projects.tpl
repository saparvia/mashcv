<%namespace file='render.tpl' import='render_list'/>

<h2 class='sectionheading'>Portfolio</h2>
${render_list(query('SELECT ?proj WHERE { ?proj a doap:Project; doap:maintainer ?subject }', subject=SUBJECT))}
