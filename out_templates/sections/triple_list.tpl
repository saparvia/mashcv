<%page args='settings'/>
<%
settings.setdefault('friendly_labels', False)
settings.setdefault('meta_triples', False)
%>

<div class='settings-panel'>
	<label><input type='checkbox' name='friendly_labels' ${'checked' if settings['friendly_labels'] else ''}>Show friendly labels</label>
	<label><input type='checkbox' name='meta_triples' ${'checked' if settings['meta_triples'] else ''}>Show meta triples</label>
</div>

<h2 class='sectionheading'>Triples in dataset</h2>
<table class='table table-striped'>
	<tr><th>Subject</th><th>Predicate</th><th>Object</th><th>Source</th></tr>
	<%
	count = 0
	format = pretty if settings['friendly_labels'] else str
	%>
	% for triple in values():
		<% if not settings['meta_triples'] and META in triple[1]: continue %>
		<tr><td>${format(triple[0])}</td><td>${format(triple[1])}</td><td>${format(triple[2])}</td><td>${pretty(triple.meta(META.sourceService))}</td></tr>
	<%
	count += 1
	%>
	% endfor
</table>
<p>
	<span>${count} triples in dataset</span>
</p>
