<%namespace file='mode_specific.tpl' import='render_single_'/>

<%def name='render_PRJA(paper)'>
	<a href='${str(paper)}'>${render_single(value(s=paper, p=DC.title))}</a>
</%def>

<%def name='render_Project(proj)'>
	<a href='${str(proj)}'>${render_single(value(s=proj, p=DOAP.name))}</a>
</%def>

<%def name='render_WorkHistory(work)'>
	${render_single(value(s=work, p=CV.jobTitle))}, ${render_single(query('SELECT ?name WHERE { ?work cv:employedIn ?company. ?company foaf:name ?name }', work=work))} (${render_single(value(s=work, p=CV.startDate))}&ndash;${render_single(value(s=work, p=CV.endDate))})
</%def>

<%def name='render_Education(study)'>
	${render_single(value(s=study, p=CV.degreeType))}, ${render_single(query('SELECT ?name WHERE { ?study cv:studiedIn ?school. ?school foaf:name ?name }', study=study))} (${render_single(value(s=study, p=CV.eduStartDate))}&ndash;${render_single(value(s=study, p=CV.eduGradDate))})
</%def>

<%def name='render_PubKey(key)'>
	<a href='${str(value(key, WOT.pubkeyAddress))}'>${render_single(value(key, WOT.hex_id))}</a>
</%def>

<%def name='render_list(results)'>
	<ul>
		% for result in results:
			<li>${render_single(result)}</li>
		% endfor
	</ul>
</%def>

<%def name='render_single(results, default=None)'>
	<%
		renderers = {
			SCIPUB.PeerReviewedJournalArticle: render_PRJA,
			DOAP.Project: render_Project,
			CV.WorkHistory: render_WorkHistory,
			CV.Education: render_Education,
			WOT.PubKey: render_PubKey,
		}
	%>
	${render_single_(results, renderers, default=default)}
</%def>
