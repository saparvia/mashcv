<%def name='render_single_(x, renderers, default=None, meta=None)'>
	<%
		typ = value(x, RDF.type, default=None)
	%>
	% if typ != None and typ in renderers:
		${renderers[typ](x, meta)}
	% else:
		% if meta:
			<span title='Source: ${str(meta[META.sourceService])}'>${str(x)}</span>
		% else:
			<span>${str(x)}</span>
		% endif
	% endif
</%def>
