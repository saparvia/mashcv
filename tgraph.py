import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

import io
import collections
import re
import itertools
import types

import rdflib
import rdflib.query
from rdflib import URIRef, Variable

from helpers import triple_uri

class Result(tuple):
	# TODO: Figure out how to make m optional
	def __new__ (cls, t, m):
		return super(Result, cls).__new__(cls, tuple(t))

	def __init__(self, t, m):
		logger.debug('Result with data %s and meta %s', t, m)
		self.meta_ = m

	def meta(self, key):
		return self.meta_[key]

class GraphInTemplate():
	def __init__(self, graph, namespaces):
		self.graph = graph
		self.namespaces = namespaces

	def query(self, query, **kwargs):
		logger.debug('Getting results for SPQRQL %s', query)
		# Perform a modified query which asks for the result of all used variables
		# These values are needed later
		# If query originally already asks for all variables, this is unnecssary. TODO: Don't do this in that case.
		query2 = re.sub(r'SELECT(.*?) ([^?])', r'SELECT * \2', query, re.IGNORECASE)
		logger.debug('Modified query to %s'%query2)

		rows = list(self.graph.query(query2, initBindings=kwargs, initNs=self.namespaces))
		
		# Figure out what triples were used for the query by looking at the "template" in the WHERE section and replacing the variables there with the actual found values
		variables, query_triples = parse_sparql(query, self.namespaces)

		results = []
		for row in rows:
			triples_used = []
			# Loop over each triple that appears in the query
			for query_triple in query_triples:
				triple = list(query_triple)
				# Replace the variables in the triples with the found values
				for i, term in enumerate(query_triple):
					if isinstance(term, Variable):
						logger.debug('Replacing term %s with %s'%(term, row[str(term)]))
						triple[i] = row[str(term)]
						logger.debug('triple is now %s'%str(triple))

				triples_used.append(tuple(triple))
			
			logger.debug('Triples used to get query result: %s'%str(triples_used))
			result = [row[variable] for variable in variables]
			meta = merge_dicts([self.metadict(triple) for triple in triples_used])
			logger.debug('Adding query result %s with meta %s', result, meta)
			results.append(Result(result, meta))

		return results

	def values(self, s=None, p=None, o=None):
		logger.debug('Looking up values for %s %s %s',s,p,o)
		wanted = []
		kwargs = {}
		# TODO: Find more elegant way to do this
		if not s:
			wanted.append('?s')
		else:
			kwargs['s'] = s
		if not p:
			wanted.append('?p')
		else:
			kwargs['p'] = p
		if not o:
			wanted.append('?o')
		else:
			kwargs['o'] = o
		return self.query('SELECT %s WHERE {?s ?p ?o}'%' '.join(wanted), **kwargs)

	# Return textual representation of given subject. If a URIRef, check if a label is found for it
	def pretty(self, subject, preferred_lang='en'):
		# Hopefully checking for these two is enough
		if isinstance(subject, list) or isinstance(subject, tuple) or isinstance(subject, types.GeneratorType) or isinstance(subject, set):
			return ', '.join([self.pretty(x, preferred_lang) for x in subject])

		if isinstance(subject, URIRef):
			labels = self.labels.preferredLabel(subject)
			if not labels:
				return str(subject)
			else:
				# Try to find a label matching the preferred language
				for _, label in labels:
					if label.language == preferred_lang:
						return label
				
				# None found, so just return the first one
				return str(labels[0][1])
				
		else:
			return str(subject)

	# Return a single result
	def value(self, s=None, p=None, o=None, default=None):
		logger.debug('Looking up value for %s %s %s',s,p,o)
		v = self.values(s, p, o)
		if v:
			logger.debug('Got result %s',v)
			return v
		else:
			logger.debug('No value found, returning default %s',(default,))
			return [Result((default,), self.metadict())]

	def metadict(self, triple=None):
		d = collections.defaultdict(lambda: None)
		if triple:
			for (p, o) in self.graph.predicate_objects(triple_uri(triple)):
				d.setdefault(p, []).append(o)
				logger.debug('Adding meta %s %s'%(p,o))
		return d

def merge_dicts(dicts):
	# TODO: Generalize this so that dicts stay dicts, defaultdicts, stay the same etc.
	new_dict = collections.defaultdict(lambda: None)
	logger.debug('Merging %d dicts',len(dicts))
	all_keys = list(itertools.chain(*[d.keys() for d in dicts]))
	for key in all_keys:
		new_dict[key] = tuple(itertools.chain(*(d[key] for d in dicts if key in d)))
	return new_dict

def parse_sparql(sparql_query, namespaces):
	# Parse the requested variables
	result = re.search(r'SELECT(.*?) [^?]', sparql_query, re.IGNORECASE)
	variables = []
	for variable in result.group(1).split():
		if not variable.startswith('?'):
			raise Exception("Encountered variable %s: parse_sparql can't handle fancy queries yet"%variable)
		variables.append(Variable(variable))

	results = re.findall('{(.*)}', sparql_query)
	if len(results) > 1 or '{' in results[0]:
		raise Exception("parse_sparql can't handle fancy queries yet")
	
	where = ''
	# Add prefixes since the parser complains otherwise
	for prefix, ns in namespaces.items():
		where += '@prefix %s: <%s> .'%(prefix, str(ns))

	where += results[0].strip()

	# Turtle wants the last dot, sparql is less picky
	if where[-1] != '.':
		where += '.'
	logger.debug('Parsing sparql where: %s'%where)


	g = rdflib.Graph()
	g.load(io.StringIO(where), format='turtle')
	return variables, g
