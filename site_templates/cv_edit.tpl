<%inherit file='master.tpl'/>
<%block name='head'>
	<script src='https://code.jquery.com/ui/1.11.4/jquery-ui.min.js'></script>
	<style>
		.panel-toggle {
			float: right;
			cursor: pointer;
		}
		.panel-remove {
			float:right;
			cursor: pointer;
		}
		.section-panel > .panel-heading {
			cursor: grab;
			cursor: -webkit-grab;
		}
		.ui-sortable-helper .panel-heading {
			cursor: grabbing;
			cursor: -webkit-grabbing;
		}
		<%block name='style'/>
	</style>
	<script>
		function init_portlet(div) {
			var panel = $(div);

			panel.addClass('panel panel-default section-panel')
				.wrapInner($('<div class="panel-body"></div>'))
				.prepend(function() {
					return $.parseHTML('<div class="panel-heading"><h3 class="panel-title">Section: '+ $(this).find('.sectionheading:first').text() + '</h3></div>');
				});

			panel.find('.settings-panel')
				.addClass('panel panel-default')
				.wrapInner($('<div class="panel-body"></div>'))
				.prepend('<div class="panel-heading">Settings</div>');

			// This is also duplicate ...
			panel.find('.settings-panel :checkbox').click(function() { rerender($(this)); });

			panel.find( "> .panel-heading" )
				.prepend( "<span class='glyphicon glyphicon-remove panel-remove'></span>")
				.prepend( "<span class='glyphicon glyphicon-minus panel-toggle'></span>");

			panel.find(".panel-toggle").click(function() {
				var icon = $( this );
				icon.toggleClass( "glyphicon-minus glyphicon-plus" );
				icon.closest( ".panel" ).find( ".panel-body" ).toggle();
			});

			panel.find('.panel-remove').click(function() {
				var icon = $( this );
				icon.closest( ".panel" ).hide('slow', function() { $(this).remove(); populate_add_section(); });
			});
		}

		// TODO: Optimize to only make needed changes
		function populate_add_section() {
			var sections = [
			% for section, label in SECTIONS:
				{section: "${section}", label: "${label}"},
			% endfor
			];

			$('#add_section_dropdown ul').empty();
			$.each(sections, function() {
				// Check if section has already been added
				// TODO: This should probably be made more specific
				if(!($('.section-'+this.section).length)) {
					var section = this.section;
					var li = $.parseHTML('<li><a href="#">'+this.label+'</a></li>');
					$(li).find('a').click(function() {
						var render_url = '/render?template=' + section + '&session_id=${SESSION_ID}';
						// TODO: Add spinner
						// TODO: Add section first, and then populate it when it has been rendered. This a) Gives more immediate visual feedback and b) Prevens user from adding the section twice (while it's loading)
						var new_section = $('<div/>').data('render', render_url).addClass('section-'+section);
						$(new_section).load(render_url, function(responseText, textStatus) {
							$('#sections').append(new_section);
							init_portlet(new_section);
							populate_add_section();
						});
					});
					$('#add_section_dropdown ul').append(li);
				}
			});
		}

		function rerender(elem_in_section) {
			var section = $(elem_in_section).closest('.section-panel');
			var render_url = section.data('render');
			var settings = encodeURIComponent(section.find('.settings-panel :input').serialize());
			var panel_body = section.find('> .panel-body')
				.load(render_url + '&settings='+settings, function(responseText, textStatus) {
					// TODO: Cleanup, duplicate code from init_portlet
					section.find('.settings-panel')
						.addClass('panel panel-default')
						.wrapInner($('<div class="panel-body"></div>'))
						.prepend('<div class="panel-heading">Settings</div>');

					// This is also duplicate ...
					section.find('.settings-panel :checkbox').click(function() { rerender($(this)); });
				});
		}

		$(function() {
			$('.panel-container > div').each(function(i, elem) { init_portlet(elem) });
			$('.panel-container').sortable({handle: '.panel-heading', cancel: '.panel-toggle'});
			populate_add_section();

			$('.settings-panel :checkbox').click(function() { rerender($(this)); });
		});
	</script>
</%block>

<div class='panel-container' id='sections'>
	<%include file='${CVTEMPLATE}.tpl'/>
</div>

<div id='controls'>
	<!-- TODO: Add spinner to show a section is loading -->
	<div class="dropup" id='add_section_dropdown'>
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Add section
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
		</ul>
	</div>
</div>
