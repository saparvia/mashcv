<%inherit file='master.tpl'/>
<%block name='head'>
	<script src='golsplash/golsplash.js'></script>
	<style>
		.radio-group label { text-align: center; }
		.radio-group label.active { background-color: rgba(0, 0, 255, 0.2); }
		span.input-group-addon img { height: 16px; } /* Feels a bit hack-y. TODO: Figure out how to get relative to container */

		#add-import-dropdown ul li img { width: 32px; height: 32px; }
		#template-radio-group label img { width: 32px; height: 32px; }
	</style>
	<script>
		$(function() {
			input_divs = [
				% for html in input_fields:
				"${html}",
				% endfor
			];

			$('#add-import-dropdown ul li a').each(function(i, elem) {
				$(elem).click(function() {
					add_importer(i);
				});
			});

		});

		function set_importer_value(importer, userid) {
			$(importer).find('input')
				.val(userid)
				.attr('readonly', true)
				.nextAll(".importer-id-status")
				.addClass("glyphicon-ok text-success");

		}
		function add_importer(importer_id_or_index, initial_id = '') {
			var importers = {}

			% for i, importer_id in enumerate(importer_ids):
			importers['${importer_id}'] = ${i};
			% endfor

			var importer;
			if(importer_id_or_index in importers) {
				importer = $('<div/>').html(input_divs[importers[importer_id_or_index]]);
			}
			else {
				importer = $('<div/>').html(input_divs[importer_id_or_index]);
			}

			if(initial_id != '') {
				set_importer_value(importer, initial_id);
			}

			$(importer).find('.oauth-link').click(function() {
				var link = $(this);
				oauth_popup(link.data('oauth-url'), function(result) {
					set_importer_value(link.parent(), result);
				});
			});

			$('#added-importers').append(importer);
		}

		function oauth_popup(oauth_url, f) {
			var popup=window.open(oauth_url);
			// TODO: Give up looking after a while
			// TODO: User closes popup manually
			var timer = window.setInterval(function() {
				if(popup.window.location.href.indexOf('result=')>-1) {
					window.clearInterval(timer);
					var url = popup.window.location.href
					var result = url.match(/result=([^&]*)/)[1];

					var other_ids_match = url.match(/other_ids=([^&]{1,})/);
					if(other_ids_match) {
						var other_ids = other_ids_match[1].split('|');
						for(var i in other_ids) {
							var parts = other_ids[i].split(':');
							add_importer(parts[0], parts[1]);
						}
					}

					popup.window.close();
					return f(result);
				}
			}, 100);
		}
	</script>
</%block>

<h2>Generate CV</h2>

<form method='get' action='${action}'>
	<div id='added-importers'>
	</div>
	
	<div class="dropdown" id='add-import-dropdown'>
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Add account
			<span class="caret"></span>
		</button>
		<ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
		% for name in importer_names:
			<li><a href='#'>${name}</a></li>
		% endfor
		</ul>
	</div>
	CV template:
	<div id='template-radio-group' class='radio-group' data-toggle='buttons'>
	% for i, (value, text) in enumerate(templates):
		<label class='btn ${"active" if i == 0 else ""}'>
			<input name='out' type='radio' value='${value}' ${'checked="checked"' if i == 0 else ''}><img class='img-fluid' src='icons/${value}.svg'/><br/><span>${text}</span>
		</label>
	% endfor
	</div>
	<br/>
	<input type='submit' onclick='splash = new GOLSplash(); splash.update();' value='Create CV'/>
</form>
