<div class='input-group'>
	<span class='input-group-addon'><img src='${icon}'/></span>
	<input name='${service}' type='text' placeholder='${placeholder}'><span class='importer-id-status glyphicon'></span>
	% if oauth_url:
	or <a href='#' class='oauth-link' data-oauth-url='${oauth_url}'>OAuth</a>
	% endif
</div>
