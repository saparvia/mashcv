<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<script src='https://code.jquery.com/jquery-2.2.3.min.js'></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<style>
			body { padding: 8px; background-color: #fafafa; }
			header h1 img { height: 64px; float:left; }
			header { border-bottom: 1px solid black; }
			header a { text-decoration: none; }
			header a:hover { text-decoration: none; }
		</style>

		<%block name='head'/>
	</head>
	<body>
		<header class='page-heading'>
			<a href='/'><h1><img src='images/logo.svg'/>MashCV<br/><small>Some clever tagline here.</small></h1></a>
		</header>
		${next.body()}
		<footer>
			TODO: Add a footer
		</footer>
	</body>
</html>
