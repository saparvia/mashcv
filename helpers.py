import hashlib
import base64
import rdflib

def triple_uri(triple):
	return rdflib.URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

def base64sha1(data):
	return base64.urlsafe_b64encode(hashlib.sha1(data).digest()).decode('utf-8')

def convert(html, to_format):
	logger.debug('Converting to format %s'%to_format)

	# Some formats require writing to a file :-( TODO: Optimize to only do it when necessary
	# According to docs this will cause problems on Windows
	suffix = '.' + to_format
	if to_format == 'pdf':
		to_format = 'latex'

	tempf = tempfile.NamedTemporaryFile(suffix=suffix)
	pypandoc.convert(html, to_format, format='html', outputfile=tempf.name)
	content = tempf.read()
	return content
