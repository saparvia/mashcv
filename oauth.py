import urllib.parse
import urllib.request
import json
import base64
import os

from secrets import secrets
def get_token(service, code, request_url):
	redirect_uri = redirect_url(service)
	data = {
		'client_id': secrets[service]['client_id'],
		'client_secret': secrets[service]['client_secret'],
		'code': code,
		'grant_type': 'authorization_code',
		'redirect_uri': redirect_uri
	}
	req = urllib.request.Request(request_url, data=bytes(urllib.parse.urlencode(data), 'utf-8'), headers={'Accept': 'application/json'})
	
	with urllib.request.urlopen(req) as f:
		contents = str(f.read(), 'utf-8')
	try:
		reply = json.loads(contents)
	except ValueError as e:
		# I did not manage to actually get Github to honor the request for json, so assume we got back a query string instead and try to parse that
		reply = urllib.parse.parse_qs(contents)

		# Assume only one single value per key
		reply = dict([(k, v[0]) for k, v in reply.items()])

	return reply

def gen_state(length=10):
	return str(base64.urlsafe_b64encode(os.urandom(length)), 'utf-8')

def auth_url(service, base_url, scope=None):
	state = gen_state()
	redirect_uri = redirect_url(service)

	return '%s?client_id=%s&state=%s&redirect_uri=%s&response_type=code%s'%(base_url, secrets[service]['client_id'], state, redirect_uri, '&scope=%s'%scope if scope else ''), state

def redirect_url(service):
	return 'http://localhost:8088/oauth/%s'%service
