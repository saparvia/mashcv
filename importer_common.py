import mako.template

import oauth

def basic_input(service, icon, placeholder, oauth_endpoint = None, oauth_scope = None):
	if oauth_endpoint:
		oauth_url, state = oauth.auth_url(service, oauth_endpoint, oauth_scope)
	else:
		oauth_url = None
		state = None

	# Using mako_template from bottle seems to not work (probably some initialization problem)
	return mako.template.Template(filename='site_templates/basic_input.tpl').render(service=service, icon=icon, placeholder=placeholder, oauth_url=oauth_url, state=state)
