import rdflib

from . import orcid2rdf

import oauth
from importer_common import basic_input

def get_profile(orcid):
	# Assume that the user identifier will be of the form below. TODO: Something better
	return orcid2rdf.get_profile(orcid), rdflib.URIRef('http://orcid.org/'+orcid)

def get_input_html():
	return basic_input('orcid', 'http://orcid.org/favicon.ico', 'ORCID', 'https://orcid.org/oauth/authorize', '/authenticate')

def get_name():
	return '<img src="http://orcid.org/favicon.ico"/>ORCID'

def oauth_handler(code, state, redir_func):
	# TODO: Check state
	reply = oauth.get_token('orcid', code, 'https://orcid.org/oauth/token')
	token = reply['access_token'] # Not actually needed
	orcid = reply['orcid']

	redir_func('/oauth_success?result=%s'%orcid)
	
