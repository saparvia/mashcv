import rdflib
import json
import urllib.request

from . import gitlab2rdf

import oauth
from importer_common import basic_input

def get_profile(gitlab_id):
	# Assume that the user identifier will be of the form below. TODO: Something better
	return gitlab2rdf.get_profile(gitlab_id), rdflib.URIRef('https://gitlab.com/api/v3/users/%s'%gitlab_id)

def get_input_html():
	return basic_input('gitlab', 'https://gitlab.com/favicon.ico', 'Gitlab ID', 'https://gitlab.com/oauth/authorize')

def get_name():
	return '<img src="https://gitlab.com/favicon.ico"/>Gitlab.com'

def oauth_handler(code, state, redir_func):
	# TODO: Check state
	reply = oauth.get_token('gitlab', code, 'https://gitlab.com/oauth/token')
	token = reply['access_token']

	with urllib.request.urlopen('https://gitlab.com/api/v3/user?access_token=%s'%(token)) as f:
		reply = json.loads(str(f.read(), 'utf-8'))

	others = []
	for identity in reply['identities']:
		# Convert GitHub numeric ID to username since that's what the GitHub APIs use
		if identity['provider'] == 'github':
			# Future-proof a bit by only getting username if the given ID seems to be numeric
			# Maybe Gitlab will return the GitHub username instead one day?
			try:
				n = int(identity['extern_uid'])
				# TODO: Check "correct" way to do this. Apparently this API is NOT official
				# TODO: This is an unauthenticated request, so will be rate-limited quite severely. Use a token to authenticate.
				with urllib.request.urlopen('https://api.github.com/user/%d'%n) as f:
					ghubreply = json.loads(str(f.read(), 'utf-8'))
					identity['extern_uid'] = ghubreply['login']
			except Exception as e:
				pass
				
		# TODO: urlencode, replace ':', '|' since they have special meaning below
		others.append('%s:%s'%(identity['provider'], identity['extern_uid']))
	
	other_ids = '|'.join(others)

	gitlab_id = reply['id']
	redir_func('/oauth_success?result=%s&other_ids=%s'%(gitlab_id, other_ids))
