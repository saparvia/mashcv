import rdflib
import urllib.request
import json

from . import gplus2rdf

import oauth
from importer_common import basic_input

def get_profile(gplus_id):
	return gplus2rdf.get_profile(gplus_id), rdflib.URIRef('https://plus.google.com/%s'%gplus_id)

def get_input_html():
	return basic_input('gplus', 'https://ssl.gstatic.com/images/branding/product/ico/google_plus_alldp.ico', 'Google+ ID', 'https://accounts.google.com/o/oauth2/v2/auth', 'https://www.googleapis.com/auth/plus.me')

def get_name():
	return '<img src="https://ssl.gstatic.com/images/branding/product/ico/google_plus_alldp.ico"/>Google+'

def oauth_handler(code, state, redir_func):
	reply = oauth.get_token('gplus', code, 'https://www.googleapis.com/oauth2/v4/token')
	token = reply['access_token']

	with urllib.request.urlopen('https://www.googleapis.com/plus/v1/people/me?access_token=%s'%(token)) as f:
		reply = json.loads(str(f.read(), 'utf-8'))

	gplus_id = reply['id']
	redir_func('/oauth_success?result=%s'%gplus_id)
