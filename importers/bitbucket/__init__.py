import rdflib
import json
import urllib.request

from . import bitbucket2rdf

import oauth
from importer_common import basic_input

def get_profile(username):
	# Assume that the user identifier will be of the form below. TODO: Something better
	return bitbucket2rdf.get_profile(username), rdflib.URIRef('https://api.bitbucket.org/2.0/users/'+username)

def get_input_html():
	return basic_input('bitbucket', 'https://bitbucket.org/favicon.ico', 'Bitbucket username', 'https://bitbucket.org/site/oauth2/authorize', 'account')

def get_name():
	return '<img src="https://bitbucket.org/favicon.ico"/>Bitbucket'

def oauth_handler(code, state, redir_func):
	# TODO: Check state
	reply = oauth.get_token('bitbucket', code, 'https://bitbucket.org/site/oauth2/access_token')
	token = reply['access_token']

	with urllib.request.urlopen('https://api.bitbucket.org/2.0/user?access_token=%s'%(token)) as f:
		reply = json.loads(str(f.read(), 'utf-8'))

	bitbucket_id = reply['username']
	redir_func('/oauth_success?result=%s'%bitbucket_id)
