import rdflib
import urllib.request
import json

from . import github2rdf

import oauth
from importer_common import basic_input

def get_profile(github_username):
	# Assume that the user identifier will be of the form below. TODO: Something better
	return github2rdf.get_profile(github_username), rdflib.URIRef('https://api.github.com/users/'+github_username)

def get_input_html():
	return basic_input('github', 'https://assets-cdn.github.com/favicon.ico', 'Github username', 'https://github.com/login/oauth/authorize')

def get_name():
	return '<img src="https://assets-cdn.github.com/favicon.ico"/>GitHub'

def oauth_handler(code, state, redir_func):
	# TODO: Check state!
	reply = oauth.get_token('github', code, 'https://github.com/login/oauth/access_token')
	token = reply['access_token']

	with urllib.request.urlopen('https://api.github.com/user?access_token=%s'%(token)) as f:
		reply = str(f.read(), 'utf-8')

	profile = json.loads(reply)
	redir_func('/oauth_success?result=%s'%profile['login']) # For some reason bottle.redirect did not work (AttributeError), so pass it as an arg instead
