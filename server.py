# Very simple HTTP server
import logging
logging.basicConfig(filename='server.log')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from urlcache import urlcache
urlcache.cache.location = 'cache'

import pathlib
import importlib
import tempfile
import base64
import os
from functools import partial
import copy
import urllib.parse
import collections

import bottle.bottle as bottle

from rdflib import Graph, Literal, URIRef, Variable, Namespace, BNode
from rdflib.namespace import FOAF, RDF, RDFS, DC, DOAP, OWL

import pypandoc

import rules
import importers
import helpers

from tgraph import GraphInTemplate

CV = Namespace('http://rdfs.org/resume-rdf/cv.rdfs#')
BIO = Namespace('http://vocab.org/bio/')
WOT = Namespace('http://xmlns.com/wot/0.1/')

META = Namespace('http://pafcu.fi/ns/meta/')
SCIPUB = Namespace('http://pafcu.fi/ns/scipub/')
COMM = Namespace('http://mashcv.eu/ns/communication/')

NAMESPACES = { 'foaf':FOAF, 'doap':DOAP, 'dc':DC, 'rdf':RDF, 'cv':CV, 'bio':BIO, 'scipub':SCIPUB, 'meta':META, 'comm':COMM, 'wot':WOT }
NS_UPPER = dict([(k.upper(), v) for k, v in NAMESPACES.items()]) # Upper-case version passed to templates

SESSIONS = {}

DEFAULT_RENDER_ARGS = {}
DEFAULT_RENDER_ARGS.update(NS_UPPER) # Namespaces
DEFAULT_RENDER_ARGS.update({ 'URIRef': URIRef, 'BNode': BNode, 'Literal': Literal }) # Types
# TODO: Get actual section names
DEFAULT_RENDER_ARGS.update({'SECTIONS': [(x.stem, x.stem.title()) for x in pathlib.Path('out_templates/sections/').glob('*.tpl')]})


# Load external data, such as RDFS schemas
# TODO: Maybe rename to internal data as this data belongs to this server (as opposed to external profile data foúnd on other servers)

# Load list of modules that can be used to import data from services
# Checks the contents of importers/ and imports each module there
IMPORTERS = dict([(x.stem, importlib.import_module('.'+x.stem, 'importers')) for x in pathlib.Path('importers').glob('./*')])

# Generate a "unique" session ID which can be used to identify a user
# NOTE: This ID is used to access sensitive information, so it must be "impossible" to guess
def gen_session_id():
	return str(base64.urlsafe_b64encode(os.urandom(10)), 'utf-8')

# Load RDF schemas and other external data
# TODO: Maybe actually call it "internal data" (beloning to this server), as opposed to external user profile data fetched from the various services
def load_extdata():
	graph = Graph()
	for path in pathlib.Path().glob('extdata/*'):
		if path.stem.startswith('.'): continue # Ignore dotfiles
		# Try to interpret the data in various formats
		# Another possibility would be to look at file extension, but the "official" file names don't always include it
		for fmt in ['xml', 'turtle']: # TODO: Add more possible formats
			logger.debug('Loading %s with format %s'%(str(path), fmt))
			try:
				graph.load(str(path), format=fmt)
				break
			except:
				logger.debug('Failed loading %s with format %s'%(str(path), fmt))
				pass
	for triple in graph:
		graph.add((helpers.triple_uri(triple), META.sourceService, Literal('ext_data')))

	rules.apply_rules(graph) # Apply some inference now, so that there's less to do later when user data is added

	return graph

EXTDATA = load_extdata()

# Save labels and make accessible in GraphInTemlate
# TODO: This is is not very nice.
GraphInTemplate.labels = Graph()
for s, p, o in EXTDATA:
	if p == RDFS.label:
		GraphInTemplate.labels.add((s, p, o))

@bottle.route('/oauth/:service')
def oauth(service):
	# TODO: Handle errors (query['error'])
	code = bottle.request.query['code']
	state = bottle.request.query['state']
	return IMPORTERS[service].oauth_handler(code, state, bottle.redirect)

# Render with chosen template
@bottle.route('/render')
def render_section():
	logger.debug('render_sectionwith query %s', str(bottle.request.query))

	session_id = bottle.request.query['session_id']
	template = bottle.request.query['template']

	# Settings are passed urlencoded
	settings = bottle.request.query.get('settings', '') # It's OK to not get any section settings
	settings = urllib.parse.parse_qs(settings)
	for k, v in settings.items():
		settings[k] = v[0] # Only one value allowed
		if settings[k] == 'on': settings[k] = True # TODO: BUG: Come up with something better. Fails with e.g. text input with text "on".
	
	try:
		session = SESSIONS[session_id]
	except KeyError:
		logger.error('session_id %s not found!',session_id)
		return ''

	graph = session['graph']
	subject = session['canonical_id']
	logger.debug('Session graph has %d triples',len(graph))
	logger.debug('canonical_id is %s', subject)

	return render_cvdata(graph, template, subject, session_id, section_settings = settings)
	
def render_cvtemplate(graph, template, subject, session_id):
	all_settings = {} # TODO: Load settings for this cvtemplate

	return render_cvdata(graph, 'cv_edit', subject, session_id, template_opts = {'CVTEMPLATE': template, 'ALL_SETTINGS': all_settings})


# Render template and pass it some useful variables
def render_cvdata(graph, template, subject, session_id, template_opts = {}, section_settings = {}):
	logger.debug('Rendering with template %s'%template)

	g = GraphInTemplate(graph, NAMESPACES)

	kwargs = {}
	kwargs.update(DEFAULT_RENDER_ARGS)

	# Helper functions
	kwargs.update({
		'query': partial(g.query),
		'values': partial(g.values),
		'pretty': partial(g.pretty),
		'value': partial(g.value),
	})

	kwargs.update({ 'SUBJECT': subject, 'SESSION_ID': session_id})

	if section_settings:
		kwargs['settings'] = collections.defaultdict(lambda: None)
		kwargs['settings'].update(section_settings)
	else:
		kwargs['settings'] = {}

	kwargs.update(template_opts)

	lookup = ['site_templates', 'out_templates/','out_templates/sections', 'out_templates/cvtemplates']
	lookup.append('out_templates/edit')
	return bottle.mako_template(template, template_lookup=lookup, **kwargs)


def get_user_profiles(services):
	user_ids = []
	g = Graph()
	# Loop over each service type and get some data from there
	for k in services:
		for v in services.getall(k):
			logger.debug('Processing arg %s=%s'%(k,v))
			if v != '':
				logger.debug('Getting data from importer %s'%k)
				newg, user_id = IMPORTERS[k].get_profile(v)
				user_ids.append(user_id)
				logger.debug('Got %d triples from importer %s about subject %s'%(len(newg), k, user_id))
				g += newg
	return g, user_ids
	
# HTML page with contents of CV
@bottle.route('/cv')
def cv():
	logger.debug('Generating CV')

	args = bottle.request.query

	# Remove non-service-name arguments
	services = copy.deepcopy(args)
	if 'out' in services: services.pop('out')
	if 'format' in services: services.pop('format')

	user_graph, user_ids = get_user_profiles(services)
	
	g = Graph()
	g += EXTDATA
	g += user_graph

	# Make all the user ids equivalent
	# Just picking the first means that it's always changing. Maybe be better to use something systematic?
	if not(user_ids):
		canonical_id = BNode()
	else:
		canonical_id = user_ids[0]
		for user_id in user_ids[1:]:
			logger.debug('Marking %s sameAs %s'%(str(user_id), str(canonical_id)))
			g.add((user_id, OWL.sameAs, user_ids[0]))

	rules.apply_rules(g)

	# External data can be removed after all rules have been applied (and labels have been saved for later use)
	# NOTE: If the code is modified to apply rules somewhere later, the external data should not be removed before that!
	g -= EXTDATA

	# Store some information for later user
	session_id = gen_session_id()
	SESSIONS[session_id] = { 'graph': g, 'canonical_id': canonical_id }

	content = bytes(render_cvtemplate(g, args['out'], canonical_id, session_id), 'utf-8')

	# Default format
	if not 'format' in args:
		args['format'] = 'html'

	if args['format'] != 'html':
		content = helpers.convert(content, args['format'])

	filename = '%s.%s'%(helpers.base64sha1(bytes(str(args), 'utf-8')), args['format'])

	with open('static/%s'%filename, 'wb') as f:
		f.write(content)

	bottle.redirect(filename) #TODO: Check correct redirect code
	
@bottle.route('/')
def generate():
	logger.debug('Showing index page')

	# Get pair of possible output formats and their names
	# TODO: No friendly names yet, but maybe some day...
	formats = [(x, x) for x in pypandoc.get_pandoc_formats()[1]]
	logger.debug('Available formats: %s'%str(formats))

	template_files = pathlib.Path().glob('out_templates/cvtemplates/*.tpl')
	templates = [(x.stem, x.stem.title()) for x in template_files if not x.stem.startswith('.')]
	logger.debug('Available templates: %s'%str(templates))

	importer_names = [x.get_name() for x in IMPORTERS.values()]

	# Escape quotes in input_fields since generate.tpl puts the content in a quoted string
	# TODO: BUG: Breaks quotes outside strings
	input_fields = [x.get_input_html().replace('"','&quot;').replace('\n',' ') for x in IMPORTERS.values()]

	return bottle.mako_template('generate', template_lookup=['site_templates'], action='/cv', templates=templates, formats=formats, input_fields=input_fields, importer_names=importer_names, importer_ids=IMPORTERS.keys())

@bottle.route('/<filename:path>')
def static(filename):
	return bottle.static_file(filename, root='static')

@bottle.error(404)
def error404(error):
	logger.info('Got 404: %s'%bottle.request.path)
	return '<html><body>Error!</body></html>'

def main():
	#bottle.debug(True) # If true template excetions are lost
	bottle.run(host='localhost', port=8088)

if __name__ == '__main__':
	main()
