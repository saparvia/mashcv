# File containing rules for deriving new information
import logging
logger = logging.getLogger(__name__)

import urllib.request
import re

from helpers import triple_uri
import hashlib

from rdflib.namespace import FOAF, RDF, DC, DOAP, OWL
from rdflib import Graph, Literal, URIRef, Namespace

META = Namespace('http://pafcu.fi/ns/meta/')
WOT = Namespace('http://xmlns.com/wot/0.1/')

COMM = Namespace('http://mashcv.eu/ns/communication/')

# Derive new information based on pre-defined rules
def apply_rules(g):
	# Iterate rules until no more triples have been added
	# Untested, may or may not work
	i = -1
	maxiter = 10
	ntriples = None
	while ntriples != len(g):
		# Make sure iteration terminates in case of some problem
		i += 1
		if i > maxiter:
			logger.warning('Iteration of rules does not seem to terminate. Stopped after %d iterations. Check your rules!')
			break

		ntriples = len(g)
		for rule in RULES:
			logging.debug('Applying rule %s', rule.__name__)
			rule(g)

# Copy metadata describing one triple to another triple
# If p is given, only data with that predicate is copied
def copy_metadata(g, src, dest, p=None):
	for _, meta_p, meta_o in g.triples((triple_uri(src), p, None)):
		g.add((triple_uri(dest), meta_p, meta_o))

# Insert foaf:name from foaf:givenName and foaf:familyName
# This needs to be corrected for various cultures
def add_name(g):
	for row in g.query('SELECT ?person ?givenName ?familyName WHERE { ?person foaf:givenName ?givenName; foaf:familyName ?familyName }', initNs={'foaf':FOAF}):
		fullname = ' '.join([str(row.givenName),str(row.familyName)])
		t = (row.person, FOAF.name, Literal(fullname))
		logger.debug('add_name: adding triple %s'%str(t))
		g.add((triple_uri(t), META.sourceService, Literal('rules.add_name')))
		copy_metadata(g, (row.person, FOAF.givenName, row.givenName), t, META.sourceService)
		copy_metadata(g, (row.person, FOAF.familyName, row.familyName), t, META.sourceService)
		g.add(t)

# Figure out what programming language someone knows based on their projects
def add_proglang(g):
	# TODO: Relax criterion to contributor
	for row in g.query('SELECT ?project ?person ?language WHERE { ?project doap:maintainer ?person ; doap:programming_language ?language }', initNs={'foaf':FOAF,'doap':DOAP,'dc':DC}):
		t = (row.person,URIRef('http://pafcu.fi/ns/programming/knowsProgLanguage'),row.language)
		logger.debug('add_proglang: adding triple %s'%str(t))
		g.add((triple_uri(t), META.sourceService, Literal('rules.add_proglang')))
		copy_metadata(g, (row.project, DOAP.maintainer, row.person), t, META.sourceService)
		copy_metadata(g, (row.project, DOAP['programming-language'], row.language), t, META.sourceService)

		g.add(t)

# Calculate sha1 sum of email since that is often used to identify people
def mbox_sha1sum(g):
	for s, o in g.subject_objects(FOAF.mbox):
		sha1sum = hashlib.sha1(bytes(o.n3(), 'utf-8')).hexdigest()
		t = (s, FOAF.mbox_sha1sum, Literal(sha1sum))
		g.add((triple_uri(t), META.sourceService, Literal('rules.mbox_sha1sum')))
		copy_metadata(g, (s, FOAF.mbox, o), t, META.sourceService)
		g.add(t)

# if s is the same as o, and o has some relation p2 with o2, then s also has that relation
# To prevent the graph from exploding, just keep one copy (i.e. remove all the equivalent cases)
# This rule could be avoided with a proper reasoning engine!
def sameas(g):
	# TODO: BUG: sourceService is added to already existing triples also
	for copy, canonical in g.subject_objects(OWL.sameAs):
		if copy == canonical:
			continue

		for p, o in list(g.predicate_objects(copy)):
			t = (canonical, p, o)
			g.add(t)
			copy_metadata(g, (copy, p, o), t)
			g.add((triple_uri(t), META.sourceService, Literal('rules.sameas')))
			g.remove((copy, p, o))
		for s, o in list(g.subject_objects(copy)):
			t = (s, canonical, o)
			g.add(t)
			copy_metadata(g, (s, copy, o), t)
			g.add((triple_uri(t), META.sourceService, Literal('rules.sameas')))
			g.remove((s, copy, o))
		for s, p in list(g.subject_predicates(copy)):
			t = (s, p, canonical)
			g.add(t)
			copy_metadata(g, (s, p, copy), t)
			g.add((triple_uri(t), META.sourceService, Literal('rules.sameas')))
			g.remove((s, p, copy))

def inverseof(g):
	for p1, p2 in g.subject_objects(OWL.inverseOf):
		# TODO: Probably more efficient ways to do this
		# TODO: BUG: sourceService is added to already existing triples also
		for s, o in g.subject_objects(p1):
			t = (o, p2, s)
			g.add(t)
			copy_metadata(g, (s,p1,o), t)

			g.add((triple_uri(t), META.sourceService, Literal('rules.inverseof')))

		for s, o in g.subject_objects(p2):
			t = (o, p1, s)
			g.add(t)
			copy_metadata(g, (s,p2,o), t)

			g.add((triple_uri(t), META.sourceService, Literal('rules.inverseof')))

def pgp_lookup(g):
	server = 'https://sks-keyservers.net'
	lookup_url = '%s/pks/lookup?op=index&search=%s&exact=on&options=mr'
	key_url = '%s/pkg/lookup?op=get&search=%s&exact=on&options=mr'
	for person, email in g.subject_objects(FOAF.mbox):
		address = str(email)

		# Remove mailto (which should be there)
		prefix = 'mailto:'
		if address.startswith(prefix):
			address = address[len(prefix):]
			source = lookup_url%(server, address)
			logger.debug('Looking for PGP key at %s', source)
			with urllib.request.urlopen(source) as f:
				reply = str(f.read(), 'utf-8')
				logger.debug('Got reply from keyserver: %s',reply)
				triples = []
				for line in reply.split('\n'):
					# TODO: Also add other identities
					parts = line.split(':')
					if parts[0] == 'pub':
						logger.debug('Result from key lookup: %s', line)
						fingerprint = parts[1]
						hex_id = fingerprint[-8:]
						length = parts[3]
						pubkeyAddress = key_url%(server, fingerprint)

						key = URIRef(pubkeyAddress) # TODO: Use fingerprint instead. What should the URI look like? Ad-hoc urn: namespace?
						triples += [
							(key, RDF.type, WOT.PubKey),
							(key, WOT.length, Literal(length)),
							(key, WOT.fingerprint, Literal(fingerprint)),
							(key, WOT.hex_id, Literal(hex_id)),
							(key, WOT.pubkeyAddress, URIRef(pubkeyAddress)),
							(person, WOT.hasKey, key)
						]

					# Add alternative mboxes
					# This may be incorrect in some cases
					if parts[0] == 'uid':
						# TODO: More robust email parsing
						# TODO: BUG: Should decode encoded chars
						address = 'mailto:'+re.search('<(.*)>', parts[1]).group(1)
						triples.append((person, FOAF.mbox, URIRef(address)))
			for triple in triples:
				g.add(triple)
				g.add((triple_uri(triple), META.sourceDocument, URIRef(source)))
				g.add((triple_uri(triple), META.sourceService, URIRef(server)))
				g.add((triple_uri(triple), META.sourceService, Literal('rules.pgp_lookup')))

# TODO: This can be covered by adding a rule handling domains, and adding the correct domain for FOAF.mbox
def mbox_type(g):
	for _, mbox in g.subject_objects(FOAF.mbox):
		triple = (mbox, RDF.type, COMM.EmailAddress)
		g.add(triple)
		g.add((triple_uri(triple), META.sourceService, Literal('rules.mbox_type')))

RULES = [add_name, add_proglang, mbox_sha1sum, sameas, inverseof, pgp_lookup, mbox_type]
